<?php

namespace Acme\Bundle\WebBundle\Controller;

use Acme\Bundle\WebBundle\Entity\Post;
use Acme\Bundle\WebBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    /**
     * @Template()
     */
    public function newAction()
    {
        $form = $this->createForm(new PostType);

        return [
            'form' => $form,
        ];
    }

    public function saveAction(Request $request)
    {
        $post = new Post;

        $form = $this->createForm(new PostType, $post);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirect($this->generateUrl('post_view',[
                'id' => $post->getId(),
            ]));
        }

        return $this->createNotFoundException();
    }

    /**
     * @Template()
     */
    public function viewAction($id)
    {
        $post = $this->getDoctrine()->getRepository('AcmeWebBundle:Post')->find($id);

        if (null === $post) {
            return $this->createNotFoundException();
        }

        $form = $this->createForm(new PostType);

        return [
            'form' => $form->createView(),
        ];
    }

    public function listAction()
    {
    }

}
